package firstLessonHomeTask;

public class First {
    public static void main(String[] args){
        int nDays = 1;
        int nHours = 21;
        int nMinutes = 5;
        int nSeconds = 16;
        int s1 = nDays * 86400 + nHours * 3600 + nMinutes * 60 + nSeconds;
        //int s1 = 1283415;
        int days = (s1/86400), hours = ((s1%86400)/3600);
        int minuts = (((s1%86400)%3600)/60), seconds = (((s1%86400)%3600)%60);

        // Дни
        System.out.print(days);
        if ((days%10) == 1){
            System.out.println(" день");
        }else if(((days%100) >= 11) && ((days%100) <= 14)){
            System.out.println(" дней");
        }else if (((days%10) > 1 && (days%10) < 5) ){
            System.out.println(" дня");
        }else {
            System.out.println(" дней");
        }

        // Часы
        System.out.print(hours);
        if ((hours%10) == 1){
            System.out.println(" час");
        }else if(((hours%100) >= 11) && ((hours%100) <= 14)){
            System.out.println(" часов");
        }else if ((hours%10) > 1 && (hours%10)<5){
            System.out.println(" часа");
        }else {
            System.out.println(" часов");
        }

        // Минуты
        System.out.print(minuts);
        if ((minuts%10) == 1){
            System.out.println(" минута");
        }else if(((minuts%100) >= 11) && ((minuts%100) <= 14)){
            System.out.println(" минут");
        }else if ((minuts%10) > 1 && (minuts%10) < 5){
            System.out.println(" минуты");
        }else {
            System.out.println(" минут");
        }

        // Секунды
        System.out.print(seconds);
        if ((seconds%10) == 1){
            System.out.println(" секунда");
        }else if(((seconds%100) >= 11) && ((seconds%100) <= 14)){
            System.out.println(" секунд");
        }else if ((seconds%10) > 1 && (seconds%10) < 5){
            System.out.println(" секунды");
        }else {
            System.out.println(" секунд");
        }
    }
}
